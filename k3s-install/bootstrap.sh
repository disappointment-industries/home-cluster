#!/bin/bash

venv=".venv"
shell=${SHELL:-bash}
install=""

if ! [ -d "$venv" ]; then
  python3 -m venv "$venv"
  install="true"
fi

source "$venv/bin/activate"

if [ "$install" != "" ]; then
  pip install wheel
  pip install ansible ansible-lint
fi

exec "$shell"
